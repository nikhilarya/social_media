<?php

ini_set('display_errors','1');
include 'includes/dbfunction.php';
$obj_dbf = new dbFunction();

if (isset($_POST['submit'])) {
	# code...
	$username = $_POST['username'];
	$emailid = $_POST['email'];
	$password = $_POST['pass'];
	$confirmPassword = $_POST['c_pass'];
	//echo "$username";
	if ($password == $confirmPassword) {
		# code...
		$email = $obj_dbf->isUserExist($emailid);
		if (!$email) {
			# code...
			$register = $obj_dbf->UserRegister($username, $emailid, $password);
			if (!$register) {
				# code...
				echo "<script>alert('Registration Not Successful')</script>";
			}
			else {
				
				echo "<script>alert('Please verify your email')</script>";
				//header("Location: login.php");
			}
		}
		else {
			echo "<script>alert('Email Already Exists')</script>";
			//header("Location: register.php");
		}
	}
	else {
		echo "<script>alert('Password not Matched')</script>";
	}
}
	
	header("Location: login.php");